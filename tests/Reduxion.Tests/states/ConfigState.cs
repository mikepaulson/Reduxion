namespace Reduxion.Tests
{
    public class ConfigState
    {
        public Environment Environment { get; set; }
        public int Limit { get; set; }

        public override string ToString()
        {
            return $"{{ Environment: {Environment}, Limit: {Limit} }}";
        }
    }
}