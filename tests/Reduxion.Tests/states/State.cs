namespace Reduxion.Tests
{
    public class State
    {
        public UserState User { get; set; }
        public ConfigState Config { get; set; }

        public override string ToString()
        {
            return $"{{\nUser: {User}\nConfig: {Config}\n}}";
        }
    }
}