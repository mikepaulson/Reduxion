using System;
using System.Collections.Generic;
using Xunit;

namespace Reduxion.Tests
{
    public class UnitTests
    {
        [Fact]
        public static void TestValueTypeActionCreateTypeMismatch()
        {
            Reduxion.Action<Enum> enumAction = Reduxion.Action<Enum>.Create("enum_type", Environment.Development);
            Exception e = Assert.Throws<InvalidOperationException>(() => ValueTypeAction<int>.Create(enumAction));
            Assert.Equal("Original action type does not match requested value type action type.", e.Message);
        }

        [Fact]
        public static void TestActionCreateTypeMismatch()
        {
            Reduxion.Action<Enum> enumAction = Reduxion.Action<Enum>.Create("enum_type", Environment.Development);
            Exception e = Assert.Throws<InvalidOperationException>(() => Reduxion.Action<string>.Create<object>("TYPE_MISMATCH", new object()));
            Assert.Equal("Cannot create an 'Action<String>' with a payload of type 'Object'.", e.Message);
        }

        [Fact]
        public static void TestCombinedReducerCreateMissingProperties()
        {
            Exception e = Assert.Throws<ArgumentException>(() => new CombinedReducer<State>(new Dictionary<string, ReducerBase>() {
                { "User", new UserReducer() }
            }));
            Assert.Equal("Expected a reducer for the following properties of 'State': 'Config'.", e.Message);
        }

        [Fact]
        public static void TestCombinedReducerCreateExtraProperties()
        {
            Exception e = Assert.Throws<ArgumentException>(() => new CombinedReducer<State>(new Dictionary<string, ReducerBase>() {
                { "User", new UserReducer() },
                { "Config", new ConfigReducer() },
                { "AuxiliaryConfig", new ConfigReducer() }
            }));
            Assert.Equal("Encountered reducers for unrecognized properties of 'State': 'AuxiliaryConfig'.", e.Message);
        }
    }
}