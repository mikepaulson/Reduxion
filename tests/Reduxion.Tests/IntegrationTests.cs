using System;
using System.Collections.Generic;
using Reduxion;
using Xunit;

namespace Reduxion.Tests
{
    public class IntegrationTests
    {
        [Fact]
        public static void ExampleTest()
        {
            var reducer = new CombinedReducer<State>(new Dictionary<string, ReducerBase>() {
                { "User", new UserReducer() },
                { "Config", new ConfigReducer() }
            });
            var store = new Store<State>(reducer);

            Console.WriteLine("Initial store state:");
            Console.WriteLine(store.State);

            Console.WriteLine("Setting configuration's environment:");
            store.Dispatch(ConfigActions.SetEnvironment(Environment.Production));
            Console.WriteLine(store.State);

            Console.WriteLine("Setting configuration's limit:");
            store.Dispatch(ConfigActions.SetLimit(11));
            Console.WriteLine(store.State);

            Console.WriteLine("Setting user's name:");
            store.Dispatch(UserActions.SetName("Frodo Baggins"));
            Console.WriteLine(store.State);

            Console.WriteLine("Setting user's email:");
            store.Dispatch(UserActions.SetEmail("frodo.baggins@middle.earth"));
            Console.WriteLine(store.State);
        }
    }
}