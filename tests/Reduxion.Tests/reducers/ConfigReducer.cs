namespace Reduxion.Tests
{
    public class ConfigReducer : Reducer<ConfigState>
    {
        public override ConfigState InitialState => new ConfigState()
        {
            Environment = Environment.Development,
            Limit = 10
        };

        public ConfigState Reduce(ConfigState state, IAction<Environment> action)
        {
            if (action.Name == ConfigActions.SET_ENVIRONMENT)
            {
                state.Environment = action.Payload;
            }

            return state;
        }

        public ConfigState Reduce(ConfigState state, IAction<int> action)
        {
            if (action.Name == ConfigActions.SET_LIMIT)
            {
                state.Limit = action.Payload;
            }

            return state;
        }
    }
}