using System;

namespace Reduxion.Example
{
    public static class ConfigActions
    {
        public const string SET_ENVIRONMENT = "CONFIG_SET_ENVIRONMENT";
        public const string SET_LIMIT = "CONFIG_SET_LIMIT";

        public static Action<Enum> SetEnvironment(Environment environment)
        {
            return Action<Enum>.Create(SET_ENVIRONMENT, environment);
        }

        public static Action<object> SetLimit(int limit)
        {
            return Action<object>.Create(SET_LIMIT, limit);
        }
    }
}