namespace Reduxion.Example
{
    public enum Environment
    {
        Development,
        Staging,
        Production
    }
}