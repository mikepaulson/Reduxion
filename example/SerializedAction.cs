using Newtonsoft.Json;

namespace Reduxion.Example
{
    class SerializedAction
    {
        public static SerializedAction Create<T>(Action<T> action) where T : class
        {
            var serializedAction = new SerializedAction();
            serializedAction.Type = action.GetType().GenericTypeArguments[0].Name;
            serializedAction.Action = JsonConvert.SerializeObject(action);

            return serializedAction;
        }

        public string Type { get; set; }
        public string Action { get; set; }
    }
}