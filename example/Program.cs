﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Reduxion.Example
{
    class Program
    {
        static void Main(string[] args)
        {
            var reducer = new CombinedReducer<State>(new Dictionary<string, ReducerBase>() {
                { "User", new UserReducer() },
                { "Config", new ConfigReducer() }
            });
            var store = new Store<State>(reducer);

            Console.WriteLine("Initial store state:");
            Console.WriteLine(store.State);

            Console.WriteLine("Setting configuration's environment:");
            store.Dispatch(ConfigActions.SetEnvironment(Environment.Production));
            Console.WriteLine(store.State);

            Console.WriteLine("Setting configuration's limit:");
            store.Dispatch(ConfigActions.SetLimit(11));
            Console.WriteLine(store.State);

            Console.WriteLine("Setting user's name:");
            store.Dispatch(UserActions.SetName("Frodo Baggins"));
            Console.WriteLine(store.State);

            Console.WriteLine("Setting user's email:");
            store.Dispatch(UserActions.SetEmail("frodo.baggins@middle.earth"));
            Console.WriteLine(store.State);

            SerializationExample(store);
        }

        private static void SerializationExample(Store<State> store)
        {
            var supportedActionTypeConfigs = new Dictionary<string, (Type Type, System.Action<Store<State>, object> Dispatch)>()
            {
                { typeof(string).Name, (typeof(Action<string>), (s, o) => s.Dispatch((Action<string>)o) ) }
            };

            Console.WriteLine("Serialization example:");

            var setNameAction = UserActions.SetName("Peregrin Took");
            var serializedAction = SerializedAction.Create(setNameAction);
            var serializedActionJson = JsonConvert.SerializeObject(serializedAction);
            Console.WriteLine("Serialized JSON:");
            Console.WriteLine(serializedActionJson);

            var deserializedAction = JsonConvert.DeserializeObject<SerializedAction>(serializedActionJson);
            var actionTypeConfig = supportedActionTypeConfigs[deserializedAction.Type];
            var deserializedStringAction = JsonConvert.DeserializeObject(deserializedAction.Action, actionTypeConfig.Type);

            Console.WriteLine("Dispatching deserialized action:");
            actionTypeConfig.Dispatch(store, deserializedStringAction);
            Console.WriteLine(store.State);
        }
    }
}
