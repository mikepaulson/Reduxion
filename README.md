# Reduxion

Reduxion is a C# library inspired by Redux for managing application state.

See [the example code](https://gitlab.com/chrisculy/Reduxion/tree/master/example) for typical usage.