using System;

namespace Reduxion
{
    public class ValueTypeAction<T> : IAction<T> where T : struct
    {
        public static ValueTypeAction<T> Create<U>(Action<U> action) where U : class
        {
            if (action.OriginalType != typeof(T))
            {
                throw new InvalidOperationException("Original action type does not match requested value type action type.");
            }

            return new ValueTypeAction<T>(action.Name, (T)Convert.ChangeType(action.Payload, typeof(T)));
        }

        private ValueTypeAction(string name, T payload)
        {
            m_name = name;
            m_payload = payload;
        }

        public string Name { get => m_name; }
        public T Payload { get => m_payload; }

        private readonly string m_name;
        private readonly T m_payload;
    }
}