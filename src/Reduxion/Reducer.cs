using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Reduxion
{
    public abstract class Reducer<T> : ReducerBase where T : new()
    {
        public Type StateType { get { return m_stateType; } }

        public abstract T InitialState { get; }

        public Reducer()
        {
            m_reduceMethodsByActionType = GetType().GetMethods().Where(IsReduceMethod).ToDictionary(
                reduceMethod => reduceMethod.GetParameters()[1].ParameterType.GetGenericArguments()[0],
                reduceMethod => reduceMethod
            );
        }

        public virtual T Reduce<U>(T state, Action<U> action) where U : class
        {
            var payloadType = action.OriginalType ?? typeof(U);
            if (m_reduceMethodsByActionType.ContainsKey(payloadType))
            {
                if (action.OriginalType != null)
                {
                    var valueTypeActionConcreteType = typeof(ValueTypeAction<>).MakeGenericType(action.OriginalType);
                    var genericCreateMethod = valueTypeActionConcreteType.GetMethod(c_createMethodName, BindingFlags.Public | BindingFlags.Static);
                    var createMethod = genericCreateMethod.MakeGenericMethod(typeof(U));
                    var actionableAction = createMethod.Invoke(null, new[] { action });
                    return (T)(m_reduceMethodsByActionType[payloadType].Invoke(this, new object[] { state, actionableAction }));
                }
                else
                {
                    return (T)(m_reduceMethodsByActionType[payloadType].Invoke(this, new object[] { state, action }));
                }
            }
            return state;
        }

        public bool SupportsActionType(Type actionType)
        {
            return m_reduceMethodsByActionType.ContainsKey(actionType);
        }

        private bool IsReduceMethod(MethodInfo method)
        {
            return method.Name == c_reduceMethodName &&
                method.GetParameters().Length == 2 &&
                method.GetParameters()[0].ParameterType == m_stateType &&
                method.GetParameters()[1].ParameterType.IsGenericType && method.GetParameters()[1].ParameterType.GetGenericTypeDefinition() == m_actionInterfaceType;
        }

        private const string c_createMethodName = "Create";
        private const string c_reduceMethodName = "Reduce";

        private readonly Type m_stateType = typeof(T);
        private readonly Type m_actionInterfaceType = typeof(IAction<>);
        private Dictionary<Type, MethodInfo> m_reduceMethodsByActionType;
    }
}