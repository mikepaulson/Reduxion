using System;

namespace Reduxion
{
    public interface ReducerBase
    {
        Type StateType { get; }
        bool SupportsActionType(Type actionType);
    }
}