namespace Reduxion
{
    public interface IAction<T>
    {
        string Name { get; }
        T Payload { get; }
    }
}