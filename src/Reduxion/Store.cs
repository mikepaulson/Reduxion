namespace Reduxion
{
    public class Store<T> where T : new()
    {
        public T State { get; private set; }

        public Store(Reducer<T> reducer)
        {
            m_reducer = reducer;
            State = m_reducer.InitialState;
        }

        public Store(Reducer<T> reducer, T initialState)
        {
            m_reducer = reducer;
            State = initialState;
        }

        public void Dispatch<U>(Action<U> action) where U : class
        {
            m_reducer.Reduce<U>(State, action);
        }

        private readonly Reducer<T> m_reducer;
    }
}