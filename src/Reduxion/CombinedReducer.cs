using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Reduxion
{
    public class CombinedReducer<T> : Reducer<T> where T : new()
    {
        public CombinedReducer(Dictionary<string, ReducerBase> reducers)
        {
            var stateProperties = typeof(T).GetProperties();
            var statePropertyNames = stateProperties.Select(stateProperty => stateProperty.Name).ToList();
            var missingProperties = statePropertyNames.Except(reducers.Keys.ToList()).ToList();
            if (missingProperties.Count != 0)
            {
                throw new ArgumentException($"Expected a reducer for the following properties of '{typeof(T).Name}': '{string.Join(", ", missingProperties)}'.");
            }

            var extraProperties = reducers.Keys.Except(statePropertyNames).ToList();
            if (extraProperties.Count != 0)
            {
                throw new ArgumentException($"Encountered reducers for unrecognized properties of '{typeof(T).Name}': '{string.Join(", ", extraProperties)}'.");
            }

            Type reducerGenericType = typeof(Reducer<>);
            m_reducerConfigs = reducers.Select(reducer =>
                {
                    var reducerConcreteType = reducerGenericType.MakeGenericType(reducer.Value.StateType);

                    return new ReducerConfig(reducer.Value, stateProperties.First(stateProperty => stateProperty.Name == reducer.Key), 
                            reducerConcreteType.GetProperty(c_initialStatePropertyName),
                            reducerConcreteType.GetMethod(c_reduceMethodName));
                }).ToList();
        }

        public override T InitialState
        {
            get
            {
                var state = new T();
                foreach (var reducerConfig in m_reducerConfigs)
                {
                    reducerConfig.StateProperty.SetValue(state, reducerConfig.ReducerInitialStateProperty.GetValue(reducerConfig.Reducer));
                }

                return state;
            }
        }

        public override T Reduce<U>(T state, Action<U> action)
        {
            var actionType = action.OriginalType ?? typeof(U);
            foreach (var reducerConfig in m_reducerConfigs.Where(r => r.Reducer.SupportsActionType(actionType)))
            {
                var substate = reducerConfig.StateProperty.GetValue(state);
                var reduceMethod = reducerConfig.ReducerReduceMethod.MakeGenericMethod(typeof(U));
                var newSubstate = reduceMethod.Invoke(reducerConfig.Reducer, new[] { substate, action });
                reducerConfig.StateProperty.SetValue(state, newSubstate);
            }

            return state;
        }

        private class ReducerConfig
        {
            public ReducerConfig(ReducerBase reducer, PropertyInfo stateProperty, PropertyInfo reducerInitialStateProperty, MethodInfo reducerReduceMethod)
            {
                Reducer = reducer;
                StateProperty = stateProperty;
                ReducerInitialStateProperty = reducerInitialStateProperty;
                ReducerReduceMethod = reducerReduceMethod;
            }

            public ReducerBase Reducer { get; }
            public PropertyInfo StateProperty { get; }
            public PropertyInfo ReducerInitialStateProperty { get; }
            public MethodInfo ReducerReduceMethod { get; }
        }

        private const string c_initialStatePropertyName = "InitialState";
        private const string c_reduceMethodName =  "Reduce";

        private List<ReducerConfig> m_reducerConfigs;
    }
}