using System;
using Newtonsoft.Json;

namespace Reduxion
{
    public class Action<T> : IAction<T> where T : class
    {
        public static Action<T> Create<U>(string name, U payload)
        {
            if (!payload.GetType().IsValueType)
            {
                if (payload is T referenceTypePayload)
                {
                    return new Action<T>(name, referenceTypePayload);
                }
                else
                {
                    throw new InvalidOperationException($"Cannot create an 'Action<{typeof(T).Name}>' with a payload of type '{typeof(U).Name}'.");
                }
            }
            else
            {
                object objectPayload = payload;
                return new Action<T>(name, (T)objectPayload, payload.GetType());
            }
        }

        [JsonConstructor]
        private Action(string name, T payload, Type originalType = null)
        {
            m_name = name;
            m_payload = payload;
            m_originalType = originalType;
        }

        public string Name { get => m_name; }
        public T Payload { get => m_payload; }
        public Type OriginalType => m_originalType;

        private readonly string m_name;
        private readonly T m_payload;
        private readonly Type m_originalType;
    }
}